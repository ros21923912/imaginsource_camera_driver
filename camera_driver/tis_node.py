#!/usr/bin/env python3
#
# This example will show you how to start a simply live stream
#

import time
import sys
import gi
import rclpy

gi.require_version("Gst", "1.0")
gi.require_version("GLib", "2.0")

from gi.repository import Gst,GLib
from sensor_msgs.msg import Image
import rclpy
from camera_driver.driver_params import tis_node_params
def print_device(device,node):
    # struc is a Gst.Structure
    struc = device.get_properties()
    node.get_logger().info(f"\tmodel:\t{struc.get_string('model')}\tserial:\t{struc.get_string('serial')}\ttype:\t{struc.get_string('type')}")


def bus_function(bus, message, user_data):
    """
    Callback for the GstBus watch
    """

    if message.type == Gst.MessageType.DEVICE_ADDED:
        device = message.parse_device_added()
        print("NEW Device")
        print_device(device)
    elif message.type == Gst.MessageType.DEVICE_REMOVED:
        device = message.parse_device_removed()
        print("REMOVED Device")
        print_device(device)

    return True

def main():
    rclpy.init()
    node=rclpy.create_node('cam_driver')
    param_listener=tis_node_params.ParamListener(node)
    params=param_listener.get_params()
    Gst.init(sys.argv)  # init gstreamer

    # this line sets the gstreamer default logging level
    # it can be removed in normal applications
    # gstreamer logging can contain verry useful information
    # when debugging your application
    # see https://gstreamer.freedesktop.org/documentation/tutorials/basic/debugging-tools.html
    # for further details
    Gst.debug_set_default_threshold(Gst.DebugLevel.WARNING)

    serial = None
    monitor = Gst.DeviceMonitor.new()
    monitor.add_filter(params.filter)
    element_type=params.TcambinName
    pipeline = Gst.parse_launch(f"tcambin name={element_type} "
                                " ! videoconvert"
                                f" ! ximagesink sync={params.ximagesSink}")

    # retrieve the bin element from the pipeline
    camera = pipeline.get_by_name(element_type)

    for device in monitor.get_devices():
        print_device(device,node)

    bus = monitor.get_bus()
    bus.add_watch(GLib.PRIORITY_DEFAULT, bus_function, None)
    monitor.start()
    node.get_logger().info("Now listening to device changes. Disconnect your camera to see a remove event. Connect it to see a connect event. Press Ctrl-C to end.\n")

    # serial is defined, thus make the source open that device
    if serial is not None:
        camera.set_property("serial", serial)

    pipeline.set_state(Gst.State.PLAYING)

    node.get_logger().info("Press Ctrl-C to stop.")

    # We wait with this thread until a
    # KeyboardInterrupt in the form of a Ctrl-C
    # arrives. This will cause the pipline
    # to be set to state NULL
    try:
        while True:
            time.sleep(1)
            node.get_logger().info("I'm alive!")
    except KeyboardInterrupt:
        monitor.stop()
        pass
    finally:
        pipeline.set_state(Gst.State.NULL)


if __name__ == "__main__":
    main()