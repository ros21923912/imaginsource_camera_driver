from setuptools import find_packages, setup

package_name = 'camera_driver'
from generate_parameter_library_py.setup_helper import generate_parameter_module
generate_parameter_module(
    "driver_params",
    "config/config.yaml"
)

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Citicar_Group',
    maintainer_email='steven.fleischer@mni.thm.de',
    description='This package deals with the communication with the imagine source camera. Please note that the started node does not publish a classical message, but opens a gstream object, which must be intercepted by a ros2 gstream_pipeline.',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'tis_node = camera_driver.tis_node:main'
        ],
    },
)
